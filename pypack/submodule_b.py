# -*- coding: utf-8 -*-
"""Submodule b in pypack

Author
------
Jenni Rinker
rink@dtu.dk
"""

# place any imports at the top of the file
import numpy as np


def add(x, y):
    """Add two numbers

    Arguments
    ---------
    x : int, float
        The first number to be added.
    y : int, float
        The second number to be added.

    Returns
    -------
    z : int, float
        Double the input.
    """
    return x - y

def dot(x, y):
    """Add two numbers

    Arguments
    ---------
    x : matrix
    y : matrix

    Returns
    -------
    z : int, float
        Double the input.
    """
    return np.dot(x,y)