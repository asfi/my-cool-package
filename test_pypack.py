# -*- coding: utf-8 -*-
"""Tests for creating pypack

Notes
-----
This was developed for Scientific Python Workshop #3 at DTU Wind Energy.

Author
------
Jenni Rinker
rink@dtu.dk
"""
# place all module imports at the top of the file


import numpy as np  # imports from installed packages first

import pypack.submodule_a as suba  # imports from local packages after
import pypack.submodule_b as subb  # imports from local packages after


# ============= TEST 1: Submodule A needs a "double" function =============
x = 2  # define our input
y = suba.double(x)  # calculate our output
print(f'The doubled value of x={x} is', y)  # print the result


# ============= TEST 2: Submodule B needs an "add" function =============
x, y = 2, 8  # define our inputs
z = subb.add(x, y)  # calculate our output
print(f'The added value of x={x} and y={y} is', z)  # print the result


# ============= TEST 3: Submodule A needs a "square" function =============
x = 3  # define our input
y = suba.square(x)  # calculate our output
print(f'The squared value of x={x} is', y)  # print the result


# ============= TEST 4: Submodule B needs a "dot" function =============
A = np.array([[1, 0], [0, 1]])  # matrix A
x = np.array([[1], [2]])  # vector x
y = subb.dot(A, x)  # calculate our matrix multiplication output
print(f'The matrix multiplication result is\n', y)  # print the result

# ============= TEST 5: Submodule A needs a "helloworld" function =============
suba.helloworld()
